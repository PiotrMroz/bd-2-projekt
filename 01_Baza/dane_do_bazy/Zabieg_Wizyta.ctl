OPTIONS(skip=1)
LOAD DATA
INFILE Zabieg_Wizyta.csv
INTO TABLE Zabieg_Wizyta
REPLACE
FIELDS TERMINATED BY ","
(
	ID_Zabieg,
     Zab,
    ID_Wizyta
)