OPTIONS(skip=1)
LOAD DATA
INFILE Wizyta.csv
INTO TABLE Wizyta
REPLACE
FIELDS TERMINATED BY ","
(
	ID_Wizyta,
	Czas_wizyty,
	Data_wizyty,
	ID_Pacjent,
	ID_Stomatolog,
	ID_Gabinet
)