	CREATE TABLE Adres( 
    ID_Adres NUMBER(9) CONSTRAINT Adres_PK PRIMARY KEY,
    Ulica VARCHAR2(255) NOT NULL,
    Nr_Domu VARCHAR2(255) NOT NULL,
    Nr_Lokalu VARCHAR2(255) NOT NULL,
    Kod_Pocztowy VARCHAR2(255) NOT NULL
);

CREATE TABLE Osoba( 
    ID_Osoba NUMBER(6) CONSTRAINT Osoba_PK PRIMARY KEY,
    Nazwisko VARCHAR2(255),
    Imie VARCHAR2(255),
	Nr_tel NUMBER (12),
    ID_Adres NUMBER(6) CONSTRAINT Osoba_Adres_FK REFERENCES Adres(ID_Adres)
);

CREATE TABLE Specjalizacja(
    ID_Specjalizacja NUMBER(6) CONSTRAINT Specjalizacja_PK PRIMARY KEY,
    Nazwa VARCHAR2(255)
);


CREATE TABLE Pracownik( 
    ID_Pracownik NUMBER(6) CONSTRAINT Pracownik_PK PRIMARY KEY,
	Wynagrodzenie NUMBER(18) NOT NULL,
    Data_zatrudnienia DATE,
	ID_Osoba NUMBER(6) CONSTRAINT Pracownik_Osoba_FK REFERENCES Osoba(ID_Osoba)
);

CREATE TABLE Stomatolog( 
    ID_Stomatolog NUMBER(6) CONSTRAINT Stomatolog_PK PRIMARY KEY,
	Lata_w_zawodzie NUMBER(12) NOT NULL,
	ID_Osoba NUMBER(6)  CONSTRAINT Stomatolog_Osoba_FK REFERENCES Osoba(ID_Osoba),
	ID_Specjalizacja NUMBER(6)  CONSTRAINT Specjalizacja_Stomatolog_FK REFERENCES Stomatolog(ID_Stomatolog)
);

CREATE TABLE Pacjent( 
    ID_Pacjent NUMBER(6) CONSTRAINT Pacjent_PK PRIMARY KEY,
	Wiek NUMBER(6),
    ID_Osoba NUMBER(6)  CONSTRAINT Pacjent_Osoba_FK REFERENCES Osoba(ID_Osoba)
);

CREATE TABLE Historia( 
    ID_Historia NUMBER(6) CONSTRAINT Historia_PK PRIMARY KEY,
   Ostatnia_w DATE NOT NULL,
   Pierwsza_w DATE NOT NULL,
   Liczba_w NUMBER (18) NOT NULL,
   ID_Pacjent NUMBER(6)  CONSTRAINT Historia_Pacjent_FK REFERENCES Pacjent(ID_Pacjent)
);

CREATE TABLE Ubezpieczenie( 
    ID_Ubez NUMBER(6) CONSTRAINT Ubez_PK PRIMARY KEY,
   Koniec_ubez DATE NOT NULL,
   Poczatek_ubez DATE NOT NULL,
   Numer_ubez NUMBER (15) NOT NULL,
   ID_Pacjent NUMBER(6)  CONSTRAINT Ubezpieczenie_Pacjent_FK REFERENCES Pacjent(ID_Pacjent)

);

CREATE TABLE Faktura( 
    ID_Faktura NUMBER(6) CONSTRAINT Faktura_PK PRIMARY KEY,
    Data_f DATE NOT NULL,
	Kwota NUMBER (12) NOT NULL,
	Nr_faktury VARCHAR2(255) NOT NULL
);

CREATE TABLE Zalecenie( 
    ID_Zalecenie NUMBER(6) CONSTRAINT Zalecenie_PK PRIMARY KEY,
	Informacja VARCHAR2(255) NOT NULL
);

CREATE TABLE Zabieg( 
    ID_Zabieg NUMBER(6) CONSTRAINT Zabieg_PK PRIMARY KEY,
	Czas_zabiegu NUMBER (18) NOT NULL,
	Rodzaj VARCHAR2(255) NOT NULL,
	ID_Zalecenie NUMBER(6)  CONSTRAINT Zabieg_Zalecenie_FK REFERENCES Zalecenie(ID_Zalecenie)
);



CREATE TABLE Problem( 
    ID_Problem NUMBER(6) CONSTRAINT Problem_PK PRIMARY KEY,
	Rodzaj VARCHAR2(255) NOT NULL
);

CREATE TABLE Gabinet( 
    ID_Gabinet NUMBER(6) CONSTRAINT Gabinet_PK PRIMARY KEY,
	Nazwa VARCHAR2(255) NOT NULL,
	Godz_otwarcia VARCHAR2(255) NOT NULL,
	Godz_zamkniecia VARCHAR2(255) NOT NULL
	
);

CREATE TABLE Wizyta( 
    ID_Wizyta NUMBER(6) CONSTRAINT Wizyta_PK PRIMARY KEY,
	Czas_wizyty VARCHAR2 (255) NOT NULL,
	Data_wizyty DATE,
	ID_Pacjent NUMBER(6)  CONSTRAINT Wizyta_Pacjent_FK REFERENCES Pacjent(ID_Pacjent),
	ID_Stomatolog NUMBER(6)  CONSTRAINT Wizyta_Stomatolog_FK REFERENCES Stomatolog(ID_Stomatolog),
	ID_Gabinet NUMBER(6)  CONSTRAINT Wizyta_Gabinet_FK REFERENCES Gabinet(ID_Gabinet)
);


CREATE TABLE Wykryty_Problem(
    CONSTRAINT Problem_Zabieg_PK PRIMARY KEY(ID_Problem, ID_Zabieg),
	ID_Problem NUMBER(6)  CONSTRAINT Wyk_Problem_FK REFERENCES Problem(ID_Problem),
    ID_Zabieg NUMBER(6)  CONSTRAINT Wyk_Zabieg_FK REFERENCES Zabieg(ID_Zabieg),
    Zab VARCHAR2 (255) NOT NULL
);

CREATE TABLE Zabieg_Wizyta(
    CONSTRAINT Zabieg_Wizyta_PK PRIMARY KEY(ID_Zabieg, ID_Wizyta),
    ID_Zabieg NUMBER(6)  CONSTRAINT Zabieg_Wizyta_FK REFERENCES Zabieg(ID_Zabieg),
    ID_Wizyta NUMBER(6)  CONSTRAINT   Wizyta_Zabieg_FK  REFERENCES Wizyta(ID_Wizyta),
    Leczony_Zab VARCHAR2 (255) NOT NULL
);




























