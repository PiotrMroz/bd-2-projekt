OPTIONS(skip=1)
LOAD DATA
INFILE Wykryty_Problem.csv
INTO TABLE Wykryty_Problem
REPLACE
FIELDS TERMINATED BY ","
(
	ID_Zabieg,
    Leczony_Zab,
	ID_Wizyta
)