OPTIONS(skip=1)
LOAD DATA
INFILE Zabieg.csv
INTO TABLE Zabieg
REPLACE
FIELDS TERMINATED BY ","
(
ID_Zabieg,
	Czas_zabiegu,
	Rodzaj,
	ID_Zalecenia
)