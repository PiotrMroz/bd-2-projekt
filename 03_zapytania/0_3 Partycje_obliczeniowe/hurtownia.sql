-- Stomatolodzy którzy najdłużej wykonywali swoje zabiegi oraz zarobki --
select s.ID_Stomatolog as "Stomatolog", AVG(w.Czas_trwania_min) as "Średni czas zabiegu", AVG(w.Cena_wizyty) as "Średni zysk"
from wizyta w 
    join stomatolog s on w.ID_Stomatolog = s.ID_Stomatolog
Group by Grouping sets(s.ID_Stomatolog, w.Czas_trwania_min, w.Cena_wizyty);