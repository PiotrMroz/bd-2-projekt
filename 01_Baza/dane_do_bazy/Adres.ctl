OPTIONS(skip=1)
LOAD DATA
INFILE Adres.csv
INTO TABLE Adres
REPLACE
FIELDS TERMINATED BY ","
(
	ID_Adres,
    Ulica,
    Nr_Domu,
    Nr_Lokalu ,
    Kod_Pocztowy 
)