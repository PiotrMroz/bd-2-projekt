
LOAD DATA
INFILE wizyta.csv
INTO TABLE Wizyta
FIELDS TERMINATED BY ","
(
	ID_Wizyta,
	Miasto_wizyty,
	Cena_wizyty,
	Czas_trwania_min,
	Typ_wizyty,
	Data_wizyty,
	Wiek_pacjenta,
	ID_Stomatolog,
	ID_Gabinet,
	ID_Rachunek,
	ID_Rodzaj_zabiegu
)