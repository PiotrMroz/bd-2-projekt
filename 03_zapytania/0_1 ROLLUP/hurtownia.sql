--wizyty gdzie czas przekraczał 60 min, wiek pacjenta był większy niż 18 lat
select w.ID_Wizyta "Id wizyty", w.Czas_trwania_min as "czas zabiegow"
from wizyta w 
where w.WIEK_PACJENTA > 18 and w.Czas_trwania_min < 60
Group by Rollup(w.ID_wizyta,w.czas_trwania_min);