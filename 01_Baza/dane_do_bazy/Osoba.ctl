OPTIONS(skip=1)
LOAD DATA
INFILE Osoba.csv
INTO TABLE osoba
REPLACE
FIELDS TERMINATED BY ","
(
	ID_Osoba,
    Nazwisko,
    Imie,
	Nr_tel,
	ID_Adres

)