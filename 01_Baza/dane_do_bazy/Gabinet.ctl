OPTIONS(skip=1)
LOAD DATA
INFILE Gabinet.csv
INTO TABLE Gabinet
REPLACE
FIELDS TERMINATED BY ","
(
	ID_Gabinet,
	Nazwa ,
	Godz_otwarcia ,
	Godz_zamkniecia 
)