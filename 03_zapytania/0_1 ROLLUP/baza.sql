--wizyty gdzie czas przekraczał 60 min, wiek pacjenta był większy niż 18 lat
select o.Nazwisko as "Pacjent", sum(z.czas_zabiegu) as "czas zabiegow"
from wizyta w 
    join pacjent p on w.ID_Pacjent = p.ID_Pacjent
    join osoba o on p.ID_Osoba = o.ID_Osoba
    join zabieg_wizyta zw on w.ID_Wiztya = zw.ID_Wizyta
    join zabieg z on zw.ID_Zabieg = z.ID_Zabieg
where p.wiek > 18 and z.czas_zabiegu > 60 
Group by Rollup(p.Nazwisko);