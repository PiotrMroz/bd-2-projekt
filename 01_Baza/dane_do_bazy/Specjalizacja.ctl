OPTIONS(skip=1)
LOAD DATA
INFILE Specjalizacja.csv
INTO TABLE Specjalizacja
REPLACE
FIELDS TERMINATED BY ","
(
	ID_Specjalizacja,
	Nazwa
)