CREATE TABLE Stomatolog( 
    ID_Stomatolog NUMBER(6) CONSTRAINT Stomatolog_PK PRIMARY KEY,
	Specjalizacja VARCHAR2(255) NOT NULL,
	Wynagrodzenie NUMBER(18) NOT NULL
);

CREATE TABLE Rachunek( 
    ID_Rachunek NUMBER(6) CONSTRAINT Rachunek_PK PRIMARY KEY,
	Nr_Rachunku VARCHAR2(255) NOT NULL
);

CREATE TABLE Rodzaj_zabiegu( 
    ID_Rodzaj_zabiegu NUMBER(6) CONSTRAINT Zabieg_PK PRIMARY KEY,
	Nazwa VARCHAR2(255) NOT NULL
);

CREATE TABLE Gabinet( 
    ID_Gabinet NUMBER(6) CONSTRAINT Gabinet_PK PRIMARY KEY,
	Ulica VARCHAR2(255) NOT NULL,
	Nr_budynku NUMBER(18) NOT NULL

);

CREATE TABLE Wizyta( 
    ID_Wizyta NUMBER(6) CONSTRAINT Wizyta_PK PRIMARY KEY,
	Miasto_wizyty VARCHAR2 (255) NOT NULL,
	Cena_wizyty NUMBER(18) NOT NULL,
	Czas_trwania_min VARCHAR2 (255) NOT NULL,
	Typ_wizyty VARCHAR2 (255) NOT NULL,
	Data_wizyty DATE,
	Wiek_pacjenta NUMBER(18) NOT NULL,
	ID_Stomatolog NUMBER(6) NOT NULL  CONSTRAINT Wizyta_Stomatolog_FK REFERENCES Stomatolog(ID_Stomatolog),
	ID_Gabinet NUMBER(6) NOT NULL  CONSTRAINT Wizyta_Gabinet_FK REFERENCES Gabinet(ID_Gabinet),
	ID_Rachunek NUMBER(6) NOT NULL  CONSTRAINT Wizyta_Rachunek_FK REFERENCES Rachunek(ID_Rachunek),
	ID_Rodzaj_zabiegu NUMBER(6) NOT NULL  CONSTRAINT Wizyta__Rodzaj_zabiegu_FK REFERENCES Rodzaj_zabiegu(ID_Rodzaj_zabiegu)
);