-- Stomatolodzy którzy najdłużej wykonywali swoje zabiegi oraz zarobki --

select o.Nazwisko as "Stomatolog", AVG(z.Czas_zabiegu) as "Średni czas zabiegu", AVG(fa.Kwota) as "Średni zysk"
from wizyta w 
    join stomatolog s on w.ID_Stomatolog = s.ID_Stomatolog
    join osoba o on s.ID_Osoba = o.ID_Osoba
    join zabieg_wizyta zw on w.ID_Wizyta = zw.ID_Wizyta
    join zabieg z on zw.ID_Zabieg = z.ID_Zabieg
    join faktura fa on w.ID_Faktura = fa.ID_Faktura
Group by Grouping sets(o.Nazwisko,z.Czas_zabiegu, fa.Kwota);