-- Średnia cena wizyty u danego stomatologa
select o.Nazwisko as "stomatolog",AVG(fa.kwota)as "Śr. koszt" 
from wizyta w
    join stomatolog s on w.ID_Stomatolog = s.ID_Stomatolog
    join osoba o on s.ID_Stomatolog = o.ID_Osoba
    join faktura fa on w.ID_Wizyta = fa.ID_Faktura
Group by CUBE(o.Nazwisko)