OPTIONS(skip=1)
LOAD DATA
INFILE Ubezpieczenie.csv
INTO TABLE Ubezpieczenie
REPLACE
FIELDS TERMINATED BY ","
(
	ID_Ubez,
   Koniec_ubez,
   Poczatek_ubez,
   Numer_ubez,
   ID_Pacjenta
	
)